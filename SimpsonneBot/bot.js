const Discord = require('discord.js');
const client = new Discord.Client();


const bonAppGifs = [
"https://tenor.com/XE7o.gif",
"https://tenor.com/AHP0.gif",
"https://tenor.com/ogCe.gif",
"https://tenor.com/U0LM.gif",
"https://tenor.com/W016.gif",
"https://tenor.com/5smB.gif"
];

const weekEndGifs = [
"https://tenor.com/Zu7I.gif",
"https://tenor.com/y5NE.gif",
"https://tenor.com/oTSe.gif"
];

const hostedLinks = {
"225711843409264655" : "http://dubois-nicolas-portfolio.planethoster.world/", // nico
"297821870265335818" : "http://warkings-universe.zd.fr/", // ujkpgs
"256493528287412224" : "https://portfolio-florian.yj.fr/", // Florian
"493796052432846848" : "Lien introuvable", // hidir
"493795284900511764" : "https://lvacreation.yo.fr/", // Laurence
"284068535347576834" : "http://gregoire-nicolle.yj.fr/", // Grégoire
"493794608870850580" : "http://perriercreatif.free.fr/", // Adeline
"493795688996667406" : "https://saunier-francois.yo.fr/", // le Pape
"184031535391965184" : "http://portfolio.nighten.fr/", // Nath'en retard
"493876433899225100" : "https://mondomaine.yo.fr/", // Dorian
"493775819861131267" : "http://thomas-cadei.yo.fr/Systeme_de_tri/", // Thomas
"493795296221069333" : "https://www.la-projets.yo.fr/", // Aurélien
"493795900016033813" : "http://sanz-eusebio.fr/", // Euz
"494838771443826702" : "https://gulzarsafi.yo.fr/", // gulz
};


client.on('ready', () => {
	client.user.setActivity("JavaScript", {type: "PLAYING"});
    // PLAYING, STREAMING, LISTENING, WATCHING

    let currTime;
    let secondes;
    let minutes;
    let heure;

    const mainChannel = client.channels.get("493774543983345688"); // Simpsonne "général" channel
    const testChannel = client.channels.get("555399957084110848"); // SRACO "bruh" channel

    setInterval(() => {
    	currTime = new Date;
    	secondes = currTime.getSeconds();
    	minutes = currTime.getMinutes();
    	heure = currTime.getHours();
    	jour = currTime.getDay();

    	if (jour !== 6 && jour !== 0) { // Only week-days
    		if (heure == 10 && minutes == 45 && secondes == 0) { // 
    			mainChannel.send("@everyone\n**C'est la pause du matin !!**");
    		}
    		else if (heure == 12 && minutes == 30 && secondes == 0) {
    			let randGif = Math.floor(Math.random() * bonAppGifs.length);
    			mainChannel.send("@everyone\n**Bon appétit !!**\n"+bonAppGifs[randGif]);
    		}
    		else if (heure == 15 && minutes == 15 && secondes == 0) {
    			mainChannel.send("@everyone\n**C'est la pause de l'après-midi !!**");

    		}
    		else if (heure == 16 && minutes == 59 && secondes == 0) {
    			if (jour == 5) {
    				let randWE = Math.floor(Math.random() * weekEndGifs.length);
    				mainChannel.send("@everyone\n**BON WEEK-END, A LUNDI ! 🎉 🎉 🎉**\n"+weekEndGifs[randWE]);
    			}
    			else {
    				mainChannel.send("@everyone\n**Bonne soirée ! A demain ! 🎉**");
    			}
    		}
    	}

    }, 1000);


});

client.on('message', (receivedMessage) => {
    if (receivedMessage.author !== client.user) { // only executes when message is not from bot itself
    	if (receivedMessage.content.startsWith("s!")) { // handles Commands
    		commandHandle(receivedMessage);
    	}
    	else { // in case of standard messages
    		messageHandle(receivedMessage);
    	}
    }
})

function commandHandle(cmdMessage) {
	let cmdFull = cmdMessage.content.substr(2);
	let argList = cmdFull.split(" ");
    // done setting

    let cmdPrimary = argList[0];
    let args = argList.slice(1);

    console.log("Command received : " + cmdPrimary);
    console.log("Arguments : " + args);

    switch(cmdPrimary) {
        case 'help':
            cmdMessage.channel.send("Voici la liste des commandes existantes :\n`s!help` => **Affiche le message actuel.**\n`s!ping` & `s!pong` => _~~Permet de jouer au ping-pong~~_ **Permet en fait de vérifier si le bot est reveillé.**\n`s!nippet [argument | help]` => **Le bot vous envoie un exemple de snippet pour l'argument défini. `s!nippet help` permet de trouver la liste des arguments valides.**\n`s!link @user` => **Le bot répond avec un lien vers le portfolio (ou l'index) de l'apprenant mentionné.**");
    	   break;
        case 'ping':
         	cmdMessage.channel.send(
    	       "Pong !"
    		);
    	   break;
    	case 'pong':
    	   cmdMessage.channel.send(
    	       "~~Ping !~~"
    		);
    	   break;
        case 'nippet':
            switch(args[0]) {
                case 'help':
                    cmdMessage.channel.send("Voici la liste des snippets disponibles :\n `for [php | js]` => Boucle **for** classique.\n`html` => Une base **html** classique.\n`form` => Un formulaire classique pour php.\n`class [php | js]` => Base syntaxique pour une classe d'**objet**.");
                    break;
                case 'for':
                    if (args[1] == "js") {
                        cmdMessage.channel.send("```js\nfor (i=0; i<5; i++) {\n  // insert code...\n}\n```");
                    }
                    else if (args[1] == "php") {
                        cmdMessage.channel.send("```php\nfor ($i=0; $i<5; $i++) {\n  // insert code...\n}\n```");
                    }
                    else {
                        cmdMessage.channel.send("Argument manquant ou incorrect. Vouliez-vous dire `php` ou `js` ?")
                    }
                    break;
                case 'html':
                    cmdMessage.channel.send("```html\n<!DOCTYPE html>\n<html lang=\"fr\">\n<head>\n   <meta charset=\"utf-8\">\n   <title>template SIMPLON html</title>\n   <link rel=\"stylesheet\" type=\"css\" href=\"style.css\">\n</head>\n<body>\n\n\n\n   <!-- SCRIPTS -->\n   <script type=\"text/javascript\" src=\"script.js\"></script>\n</body>\n</html>\n```");
                    break;
                case 'form':
                    cmdMessage.channel.send("```html\n<form method=\"POST\" action=\"index.php\">\n\n   <label for=\"firstForm\">Pseudonyme</label>\n   <input type=\"username\" name=\"username\" id=\"firstForm\">\n\n   <label for=\"secondForm\">Password</label>\n   <input type=\"password\" name=\"password\" id=\"secondForm\">\n\n   <button type=\"submit\">Submit</button>\n\n</form>\n```");
                    break;
                case 'class':
                    if (args[1] == "js") {
                        cmdMessage.channel.send("```js\nclass ClassName {\n\n  constructor(parameter) {\n    this.parameter = parameter;\n  }\n\n  someMethod() {\n    // insert code here...\n  }\n\n}```");
                    }
                    else if (args[1] == "php") {
                        cmdMessage.channel.send("```php\n// Work in Progres...\n```");
                    }
                    else {
                        cmdMessage.channel.send("Argument manquant ou incorrect. Vouliez-vous dire `php` ou `js` ?")
                    }
                    break;
                default:
                    cmdMessage.channel.send("Désolé, aucun snippet n'est reconnu. Pour en avoir une liste, tapez `s!nippet help` !");
                    break;
            }
            break;
        case 'link':
        	mentionnedID = args[0].replace(/[\\<>@#&!]/g, ""); // mentionnedID contient l'ID sous forme BRUT
        	if (!args[0]) { // Argument manquant
        		cmdMessage.channel.send("Argument manquant. Veuillez utiliser la syntaxe : `s!link @user`")
        	}
        	else if (!hostedLinks[mentionnedID]) { // Argument inexistant dans l'array "hostedLink"
        		// (usage de la forme ID brut car c'est la seule chose qui est universelle selon les surnoms des utilisateurs)
        		cmdMessage.channel.send("Argument invalide : Vérifiez bien que vous avez **mentionné** un apprenant !")
        	}
        	else {
        		userMentionned = cmdMessage.guild.members.get(mentionnedID); // Chopper l'objet "GuildMember" de la mention

        		if (userMentionned == undefined) { // if user mentionned via full tag isn't on server
        			// essentially here to avoid bot crashing
        			cmdMessage.channel.send("Désolé, l'utilisateur que vous avez mentionné semble être absent de ce serveur.");
        		}
        		else if (userMentionned.nickname == null) { // si l'utilisateur n'a pas de surnom => afficher l'username
        			showName = userMentionned.user.username;
        			cmdMessage.channel.send("**\"" + showName + "\"** => " + hostedLinks[mentionnedID]);
        		}
        		else { // si l'utilisateur a un surnom => afficher ce surnom
        			showName = userMentionned.nickname;
        			cmdMessage.channel.send("**\"" + showName + "\"** => " + hostedLinks[mentionnedID]);
        		}
        	}
        	break;
        default:
            cmdMessage.channel.send("Commande introuvable. Veuillez vérifier la commande avec `s!help`.");
            break;
    }
}

function messageHandle(receivedMessage) {
	if (receivedMessage.content.includes('octogone')) {
		receivedMessage.react('🙃');
	};
	if (receivedMessage.content.includes('omfg')) {
		receivedMessage.react('549526678636986368');
	}

}

bot_secret_token = "NTU1NzMxMTkwOTg5MTkzMjE3.D2vdKA.3jsHszK9yTVRZhTYOIDK7Z36p80"

client.login(bot_secret_token)

// receivedMessage.guild.emojis.get('549526678636986368')
// 🙃
